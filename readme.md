# Arch Linux installation - BTRFS, encryption LUKS2, swapfile, hibernate
Although the Arch linux wiki is very comprehensive on the installation process topic, there is sometimes too many information and it takes some time to extract the necessary steps for the intended installation. My intention was to make the system as most secure as possible by encryption and use btrfs, mostly for it's snapshots ability. I've come across 3 possible options how to do the encryption:
1. Leave the `/boot` volume unencrypted. For this setup you can use the EFI partition directly and host your boot loader files along with EFI ones. This is how systemd bootloader does it. This setup is prone to the evil maid attack though and mixing the boot loader and EFI files especially on multi boot system creates a mess.
2. Grub (and I was not able to find any other boot loader that does) does not support LUKS2 yet (there is some experimental support, but I was not able to get it working). So the next option would be to have the whole `/root` with `/boot` encrypted with LUKS1. But as I wanted to have the LUKS2 enryption, this was not an option for me.
3. Encrypt `/boot` with LUKS1 and `/root` with LUKS2. The steps below are for this setup.

## Basic system
### Installing remotely (optional)
Boot live iso and enable ssh.
```sh
passwd #set the password
systemctl start sshd
```
From the other machine:
```sh
ssh root@archiso
```

### Preparing the volumes and filesystem
- Create the volumes - use `cfdisk` to create a structure. Create dedicated volume for EFI - at least 260MB, boot - 200M and your root filesystem. After the installation it might look like this:
```text
NAME          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
nvme0n1       259:0    0 465.8G  0 disk  
├─nvme0n1p1   259:1    0   300M  0 part  /efi
├─nvme0n1p2   259:3    0   200M  0 part`  
│ └─cryptboot 254:1    0   198M  0 crypt /boot
└─nvme0n1p3   259:4    0 465.3G  0 part  
  └─cryptroot 254:0    0 465.3G  0 crypt /root
```
- Encrypt the volumes - boot with LUKS1, root with LUKS2, and create filesystems
```sh
cryptsetup -y -v luksFormat --type luks1 /dev/nvme0n1p2
cryptsetup luksFormat /dev/nvme0n1p3

cryptsetup luksOpen /dev/nvme0n1p2 cryptboot
cryptsetup luksOpen /dev/nvme0n1p3 cryptroot

mkfs.ext4 /dev/mapper/cryptboot
mkfs.btrfs -m single -L arch /dev/mapper/cryptroot
mkfs.vfat /dev/nvme0n1p1
```
- Create btrfs subvolumes
```sh
mount -o compress=lzo /dev/mapper/cryptroot /mnt

cd /mnt
btrfs su cr @
btrfs su cr @boot
btrfs su cr @home
btrfs su cr @log
btrfs su cr @pkg
btrfs su cr @tmp
btrfs su cr @snapshots
btrfs su cr @swap
cd ..
umount /mnt
```
- Mount everything
```sh
mount -o compress=lzo,subvol=@ /dev/mapper/cryptroot /mnt

cd /mnt
mkdir -p {boot,efi,home,swap,.snapshots,var/{log,cache/pacman/pkg,tmp}}

mount -o compress=lzo,subvol=@home /dev/mapper/cryptroot /mnt/home
mount -o compress=lzo,subvol=@log /dev/mapper/cryptroot /mnt/var/log
mount -o compress=lzo,subvol=@pkg /dev/mapper/cryptroot /mnt/var/cache/pacman/pkg
mount -o compress=lzo,subvol=@tmp /dev/mapper/cryptroot /mnt/var/tmp
mount -o compress=lzo,subvol=@snapshots /dev/mapper/cryptroot /mnt/.snapshots
mount -o compress=lzo,subvol=@swap /dev/mapper/cryptroot /mnt/swap
mount /dev/mapper/cryptboot /mnt/boot
mount /dev/nvme0n1p1 /mnt/efi
```
### Installation
- Install the base system and chroot into it
```sh
pacstrap -i /mnt base base-devel linux linux-firmware vim networkmanager btrfs-progs grub efibootmgr sudo git intel-ucode reflector udev
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
ln -s /usr/share/zoneinfo/Europe/Bratislava /etc/localtime
hwclock --systohc
vim /etc/locale.gen # Uncomment en_US.UTF-8 UTF-8 line
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "arch" > /etc/hostname
vim /etc/hosts # Configure 127.0.0.1 and ::1 lines accordingly
```
- Create crypto files. Unless you want to type the passwords 3 times during the boot:
```sh
dd bs=512 count=4 if=/dev/urandom of=/crypto_keyfile.bin
cryptsetup luksAddKey /dev/nvme0n1p2 /crypto_keyfile.bin
cryptsetup luksAddKey /dev/nvme0n1p3 /crypto_keyfile.bin 
```
- Create the initial ramdisk environment. Edit mkinitcpio configuration:
  - Add the cryptofile: `FILES=(/crypto_keyfile.bin)`
  - And add hooks: `HOOKS=(... encrypt btrfs resume)`
```sh
vim /etc/mkinitcpio.conf # Add the stuff described above
mkinitcpio -p linux
```
- Configure grub bootloader to support encryption:
  - `GRUB_CMDLINE_LINUX="cryptdevice=UUID={YOUR-ENCRYPTED-VOLUME-UUID}:cryptroot"`
  - `GRUB_ENABLE_CRYPTODISK=y`
```sh
grub-install --target=x86_64-efi --bootloader-id=arch --efi-directory=/efi --boot-directory=/boot /dev/nvme0n1
vim etc/default/grub # Add the stuff described above
grub-mkconfig -o /boot/grub/grub.cfg
```
- You can open only one enrypted volume with during the boot loader boot process and it should be your `/root` partition. For everything else use crypttab. And yes, it is necessary to open the boot partition once again and mount it properly as it was not opened by the loaded OS thus not present in `/dev/mapper`.
Add to `/etc/crypttab`
```
cryptboot UUID={YOUR-ENCRYPTED-BOOT-PARTITION-UUID} /crypto_keyfile.bin luks
```
- Create a user profile. We have not enabled `root` login, so it's disabled by default.
```sh
useradd --create-home pavel
passwd pavel
usermod --append --groups wheel pavel
visudo # Uncomment wheel group
```
- Enable the network manager
```sh
systemctl enable NetworkManager.service
```
- Sort the pacman mirrors (you can also use `--country` option to limit to specific countries)
```sh
reflector --verbose -l 200 -n 20 -p http --sort rate --save /etc/pacman.d/mirrorlist
```
- Reboot
### Post installation
- Connect wifi
```sh
nmcli device wifi connect SSID_or_BSSID --ask
```
- Install yay (or other prefered aur helper):
```sh
git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -sri
```
- Install missing drivers based on the output of mkinitcpio. Mine was giving such warnings:
```
==> WARNING: Possibly missing firmware for module: aic94xx
==> WARNING: Possibly missing firmware for module: wd719x
==> WARNING: Possibly missing firmware for module: xhci_pci
```
- Fix it:
```sh
yay -S upd72020x-fw aic94xx-firmware wd719x-firmware
sudo mkinitcpio -p linux
```
- Install timeshift
```sh
yay -S timeshift
```
- Create a snapshot before messing up:
```sh
timeshift --create --comments "Initial snapshot" --btrfs
```

## Nvidia graphics
- Enable multilib by uncommenting those lines in `/etc/pacman.conf`
```sh
[multilib]
Include = /etc/pacman.d/mirrorlist
```
- Istall those packages
```sh
yay -S nvidia-open nvidia-utils lib32-nvidia-utils egl-wayland
```
- Remove kms from the HOOKS array in /etc/mkinitcpio.conf and regenerate the initramfs.


## Swap file
- Create swapfile
```sh
sudo truncate -s 0 /swap/swapfile
sudo chattr +C /swap/swapfile 
sudo btrfs property set /swap/swapfile compression none
sudo chmod 600 /swap/swapfile 
sudo fallocate -l 32G /swap/swapfile 
sudo swapon /swap/swapfile
```
- Edit `/etc/fstab`:
```
/swap/swapfile					none 			swap 		defaults 0 0
```
- Calculate offset. First compile `btrfs_map_physical` according https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate
```sh
echo (btrfs_map_physical /swap/swapfile | head -n2 | tail -n1 | awk '{print $6}') / (getconf PAGESIZE) | bc
```
- Edit grub config - add to `GRUB_CMDLINE_LINUX`:
```
resume=/dev/mapper/cryptroot resume_offset=12345 # From the previos step
```
- Edit `systemd-hibernate.service` and `systemd-logind.service` otherwise it will fail with _Not enough swap space for hibernation_ and _Failed to find location to hibernate to: Function not implemented_ error in journal. Add the block below to both of the configurations:
```
[Service]       
Environment=SYSTEMD_BYPASS_HIBERNATION_MEMORY_CHECK=1
```
```sh
sudo systemctl edit systemd-logind.service
sudo systemctl edit systemd-hibernate.service
```

## Using secure boot
To enable secure boot, you need to have the boot loader signed and trusted. There are two options if you want to have secure boot enabled:
1. Sign it your self and add the trust root into UEFI - this will be more secure as you wan't rely on Microsoft trust root. But as I have dual boot with Windows, I cannot just remove the Microsoft one.
2. Use the signed preloader from aur. See https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface/Secure_Boot

## Install hyprland (Optional)
```sh
sudo pacman -S hyprland hypridle hyprpaper mako waybar wofi
```

### Additional packages
```sh
# bluetooth
yay -S blueman bluez bluez-libs bluez-utils 

# browser
yay -S brave-bin librewolf-bin

# fonts
yay -S ttf-jetbrains-mono ttf-jetbrains-mono-nerd

# utilities
yay -S eza bat fastfetch fzf lshw starship thefuck tmux zoxide zsh
```

## Install additional software - this is what I use
- Deja dup backups. I have it configured to do a daily backup of my home folder and upload to my NAS server:
```sh
yay -S deja-dup
```

- Security, see: https://wiki.archlinux.org/index.php/AppArmor#Installation
```sh
yay -S apparmor arch-audit
```
- Bluetooth
```sh
yay -S bluez bluez-utils gnome-bluetooth
sudo systemctl enable bluetooth
```
- SD card reader support
```sh
yay -S rts_bpp-dkms-git
```

- Btrf grub - support to boot a snapshot: https://github.com/Antynea/grub-btrfs

## Enable hibernation on lid close
- Edit logind configuration
```sh
sudo vim /etc/systemd/logind.conf # uncomment HandleLidSwitch=hibernate
sudo systemctl kill -s HUP systemd-logind # to apply changes
```
